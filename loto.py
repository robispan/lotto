import random


def loto():
    nums = []
    while len(nums) < 8:
        num = random.randint(1, 39)
        if str(num) not in nums:
            nums.append(str(num).zfill(2))

    nums = sorted(nums)
    nums = ', '.join(nums)
    return nums


print loto()
